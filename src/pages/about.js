import React from "react";
import Layout from "../components/Layout";
import { Typography } from "@material-ui/core";

const About = () => {
  return (
    <Layout>
      <Typography variant="h2" align="center">
        About Me
      </Typography>
      <Typography variant="body1" align="center">
        My name is "muhammad zakie". I am a software developer who loves to
        build web applications using modern technologies such as React and
        Node.js.
      </Typography>
    </Layout>
  );
};

export default About;
