import * as React from "react";
import Layout from "../components/Layout";
import { Button } from "@material-ui/core";

const styles = {
  container: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    height: "80vh",
    flexDirection: "column",
  },
};

export default function Home() {
  return (
    <Layout>
      <div style={styles.container}>
        <h1>Hello World!</h1>
        <Button
          variant="contained"
          color="secondary"
          href="https://www.youtube.com/@programmercintasunnah9177/videos"
          target="_blank"
        >
          Click me
        </Button>
      </div>
    </Layout>
  );
}
