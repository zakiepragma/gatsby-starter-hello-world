import { Link } from "gatsby";
import { AppBar, Toolbar, Typography, IconButton } from "@material-ui/core";
import React from "react";

const Navbar = () => {
  return (
    <AppBar position="static">
      <Toolbar>
        <Typography variant="h6" style={{ flexGrow: 1 }}>
          My Website
        </Typography>
        <ul style={{ display: "flex", listStyleType: "none", margin: 0 }}>
          <li style={{ marginRight: "1rem" }}>
            <Link to="/" style={{ color: "inherit", textDecoration: "none" }}>
              Home
            </Link>
          </li>
          <li>
            <Link
              to="/about"
              style={{ color: "inherit", textDecoration: "none" }}
            >
              About
            </Link>
          </li>
        </ul>
      </Toolbar>
    </AppBar>
  );
};

export default Navbar;
